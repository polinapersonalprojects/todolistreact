import Button from '@mui/material/Button';
import './Filter.css';
import { Box } from "@mui/material";

const Filter = ({ currentTodos, completedTodos, allTodos }) => {

  return (
      <Box className="navigation">
        <Button
            variant="outlined"
            onClick={allTodos}
        >
          ALL
        </Button>
        <Button
            variant="outlined"
            onClick={completedTodos}
        >
          COMPLETED
        </Button>
        <Button
            variant="outlined"
            onClick={currentTodos}
        >
          CURRENT
        </Button>
      </Box>
  )
}






export default Filter;
