import Item from './Item/Item';
import { Box, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import LibraryAddCheckIcon from '@mui/icons-material/LibraryAddCheck';
import './ListItems.css';

const ListItems = ({
                     todos,
                     completeAllTodos,
                     toggleTodo,
                     editTodo,
                     deleteTodo,
                     addEditTodo,
                     deleteAll,
                     completed,
                     showAll,
                     current
                   }) => {
  const isAllCompleted = todos.filter(todo => todo.done === true).length;
  const hasTodos = todos.length === 0;


  return (
      <Box className="wrapper_listitem">
        <Box className="wrapper_listitem_btn">
          <IconButton
              color={isAllCompleted ? "primary" : "default"}
              onClick={() => completeAllTodos()}
              disabled={hasTodos}
          >
            <LibraryAddCheckIcon/>
          </IconButton>
          <IconButton
              aria-label="delete"
              size="large"
              color={isAllCompleted ? "error" : "default"}
              onClick={() => deleteAll()}
              disabled={hasTodos}
          >
            <DeleteIcon fontSize="inherit"/>
          </IconButton>
        </Box>

        <ul className="list_item">
          {showAll && (
              todos.map((todo, index) => {
                return (
                    <Item
                        todo={todo}
                        index={index}
                        toggleTodo={toggleTodo}
                        editTodo={editTodo}
                        addEditTodo={addEditTodo}
                        deleteTodo={deleteTodo}
                    />
                )
              })
          )}
          {completed && (
              todos.filter(todo => todo.done === true).map((todo, index) => {
                return (
                    <Item
                        todo={todo}
                        index={index}
                        toggleTodo={toggleTodo}
                        editTodo={editTodo}
                        addEditTodo={addEditTodo}
                        deleteTodo={deleteTodo}
                    />
                )
              })
          )}
          {current && (
              todos.filter(todo => todo.done === false).map((todo, index) => {
                return (
                    <Item
                        todo={todo}
                        index={index}
                        toggleTodo={toggleTodo}
                        editTodo={editTodo}
                        addEditTodo={addEditTodo}
                        deleteTodo={deleteTodo}
                    />
                )
              })
          )}
        </ul>
      </Box>

  );
};

export default ListItems;